﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnitTestDemo.Model;

namespace UnitTestDemo.Infrastructure.Json
{
    public class JsonProvider
    {
        public static T LoadJson<T>(string path)
        {
            using (var r = new StreamReader(path))
            {
                var json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(json);
            }
        }
    }
}
