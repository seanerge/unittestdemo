﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestDemo.Model
{
    public class ItemSource
    {
        public List<Item> Items { get; set; }
    }
}
