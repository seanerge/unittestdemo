﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitTestDemo.Infrastructure.Json;
using UnitTestDemo.Model;

namespace UnitTestDemo.Ch3
{
    public class ItemDA : IItemDA
    {
        private const string ItemSourcePath = "app/ItemSource.json";

        public List<Item> GetItems()
        {
            var itemSource = JsonProvider.LoadJson<ItemSource>(ItemSourcePath);

            return itemSource?.Items;
        }

    }
}
