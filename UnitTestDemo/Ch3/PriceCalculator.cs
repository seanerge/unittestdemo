﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestDemo.Ch3
{
    public class PriceCalculator : IPriceCalculator
    {
        public decimal GetDefaultPrice(string item)
        {
            return item == "Iphone" ? 10m : 0m;
        }

        public decimal GetExtendPrices(string item, int qty)
        {
            return this.GetDefaultPrice(item) * qty;
        }
    }
}
