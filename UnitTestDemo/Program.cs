﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using UnitTestDemo.Ch3;
using UnitTestDemo.Infrastructure.Json;
using UnitTestDemo.Model;

namespace UnitTestDemo
{
    public class Program
    {
        static void Main()
        {
            var result = new ItemBusiness(new PriceCalculator(), new ItemDA()).GetAppleItems();
            var json = JsonConvert.SerializeObject(result);
            Console.WriteLine(json);

            Console.ReadKey();
        }
    }
}
