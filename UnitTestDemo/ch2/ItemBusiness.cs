﻿namespace Ch2
{
    public class ItemBusiness
    {
        public decimal GetDefaultPrice(string item)
        {
            return item == "Iphone" ? 10m : 0m;
        }

        public decimal GetExtendPrices(string item, int qty)
        {
            return this.GetDefaultPrice(item) * qty;
        }
    }
}